#include <cassert>

#include "kernels.h"

/******************************************************************************/
//  addGrids
/******************************************************************************/
void addGrids(
    GridType a,
    const GridType b[],
    const unsigned nrThreads)
{
    #pragma omp parallel for
    for (int i = 0; i < GRID_V * GRID_U * POLARIZATIONS / 4; i ++) {
        __m256 sum = ((__m256 *) b)[i];

        for (unsigned g = 1; g < nrThreads; g ++) {
            sum = _mm256_add_ps(sum, ((__m256 *) b[g])[i]);
        }

        ((__m256 *) a)[i] = sum;
    }
}



/******************************************************************************/
//  wprojection
/******************************************************************************/
void wprojection(
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
    assert(ORDER == ORDER_W_OV_OU_V_U);

    #pragma omp parallel
    {
        GridType *localGrid = &grid[omp_get_thread_num()];

        #if MODE == MODE_OVERSAMPLE
        #pragma omp for schedule(dynamic)
        for (int bl = 0; bl < BASELINES; bl ++) {
            unsigned v_end  = supportPixelsUsed[bl].y;

            for (unsigned v = 0; v < v_end; v ++) {
                for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
                    float uc = uvw[bl][0][ch].x;
                    float vc = uvw[bl][0][ch].y;
                    unsigned wc = (unsigned) uvw[bl][0][ch].z;
                    unsigned grid_u = (unsigned) uc;
                    unsigned grid_v = (unsigned) vc;
                    float      u_frac = uc - grid_u;
                    float      v_frac = vc - grid_v;
                    unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
                    unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
                    unsigned u_end  = supportPixelsUsed[bl].x;

                    __m256 vis_ = _mm256_load_ps((const float *) &visibilities[bl][0][ch][0]);
                    __m512 vis  = _mm512_castps256_ps512(vis_);
                    vis = (__m512) _mm512_inserti64x4((__m512i) vis, (__m256i) vis_, 0xF);
                    __m512 *gridptr = (__m512 *) &(*localGrid)[grid_v + v][grid_u][0];

                    float2 *support_ptr = &support[wc][ov][ou][v][0];
                    for (unsigned u = 0; u < u_end; u += 2) {
                        __m256 weight_r1 = _mm256_set1_ps(support_ptr[u].x);
                        __m256 weight_i1 = _mm256_set1_ps(support_ptr[u].y);
                        __m256 weight_r2 = _mm256_set1_ps(support_ptr[u+1].x);
                        __m256 weight_i2 = _mm256_set1_ps(support_ptr[u+1].y);
                        __m512 weight_r = _mm512_castps256_ps512(weight_r1);
                        __m512 weight_i = _mm512_castps256_ps512(weight_i1);
                        weight_r = (__m512) _mm512_inserti64x4((__m512i) weight_r, (__m256i) weight_r2, 0xF);
                        weight_i = (__m512) _mm512_inserti64x4((__m512i) weight_i, (__m256i) weight_i2, 0xF);

                        __m512 t7 = _mm512_mul_ps(weight_r, vis);
                        __m512 t6 = _mm512_mul_ps(weight_i, vis);
                        __m512 t8 = _mm512_permute_ps(t6, 0xB1);
                        __m512 t1 = _mm512_set1_ps(1);
                        __m512 t9 = _mm512_fmaddsub_ps(t1, t7, t8);
                        gridptr[u] = _mm512_add_ps(gridptr[u], t9);
                    }
                }
            }
        }
        #endif

        #if MODE == MODE_SIMPLE
        #pragma omp for schedule(dynamic)
        for (int bl = 0; bl < BASELINES; bl ++) {
            unsigned u_end  = supportPixelsUsed[bl].x;
            unsigned v_end  = supportPixelsUsed[bl].y;

            for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
                unsigned grid_u = (unsigned) nearbyintf(uvw[bl][0][ch].x);
                unsigned grid_v = (unsigned) nearbyintf(uvw[bl][0][ch].y);

                for (unsigned v = 0; v < v_end; v ++) {
                    for (unsigned u = 0; u < u_end; u ++) {
                        float2 weight = support[v][u];

                        for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
                            float2 prod   = visibilities[bl][0][ch][pol] * weight;
                            float2 &pixel = (*localGrid)[grid_v + v][grid_u + u][pol];
                            pixel += prod;
                        }
                    }
                }
            }
        }
        #endif
    }
}


/******************************************************************************/
//	awprojection
/******************************************************************************/
#if MODE == MODE_ATERM
void awprojection(
    const unsigned bl,
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
    unsigned v_end  = supportPixelsUsed[bl].y;

    GridType *localGrid = &grid[omp_get_thread_num()];

	for (unsigned v = 0; v < v_end; v ++) {
		for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
			float uc = uvw[bl][0][ch].x;
			float vc = uvw[bl][0][ch].y;
            unsigned wc = (unsigned) uvw[bl][0][ch].z;
			unsigned grid_u = (unsigned) uc;
			unsigned grid_v = (unsigned) vc;
			float	   u_frac = uc - grid_u;
			float	   v_frac = vc - grid_v;
			unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
			unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
			unsigned u_end  = supportPixelsUsed[bl].x;

			float2 visXX = visibilities[bl][0][ch][0];
			float2 visXY = visibilities[bl][0][ch][1];
			float2 visYX = visibilities[bl][0][ch][2];
			float2 visYY = visibilities[bl][0][ch][3];

			for (unsigned u = 0; u < u_end; u ++) {
				float2 weight = support[wc][v][ov][u][ou];
				(*localGrid)[grid_v + v][grid_u + u][0] += visXX * weight;
				(*localGrid)[grid_v + v][grid_u + u][1] += visXY * weight;
				(*localGrid)[grid_v + v][grid_u + u][2] += visYX * weight;
				(*localGrid)[grid_v + v][grid_u + u][3] += visYY * weight;
			}
		}
	}
}
#endif
