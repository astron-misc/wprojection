/******************************************************************************/
//  Includes
/******************************************************************************/
#include <omp.h>
#include <cuda.h>

#include "defines.h"
#include "init.h"
#include "types.h"
#include "kernels.h"
#include "visualize.h"
#include "common.h"
#include "config.h"

#if defined(ENABLE_ATERM)
#include "cufft.h"
#endif

#if defined(HAVE_POWERSENSOR)
#include "powersensor.h"
#endif

/******************************************************************************/
//  AW-Projection parameters
/******************************************************************************/
#define AWPROJECTION_INIT_KERNEL 0
#if defined(ENABLE_ATERM_FULL)
#define AWPROJECTION_COPY_KERNEL 1
#define AWPROJECTION_FFT_KERNEL  1
#else
#define AWPROJECTION_COPY_KERNEL 1
#define AWPROJECTION_FFT_KERNEL  0
#endif
#define AWPROJECTION_JOBSIZE     128

/******************************************************************************/
//  doCuda
/******************************************************************************/
void doCuda() {
    // Initialize CUDA
    const char* cuda_device_cstr = getenv("CUDA_DEVICE");
    int device = cuda_device_cstr ? atoi(cuda_device_cstr) : 0;
    checkCudaCall(cudaSetDevice(device));

    // Initialize grid
    HostMemory h_grid(sizeof(wpg::GridType));
    DeviceMemory d_grid(sizeof(wpg::GridType));
    checkCudaCall(cudaMemset(d_grid, 0, sizeof(wpg::GridType)));

    // Streams
    Stream executestream;
    Stream iostream;

    // Initialize supportPixelsUsed
    HostMemory h_supportPixelsUsed(BASELINES * sizeof(uint2));
    DeviceMemory d_supportPixelsUsed(BASELINES * sizeof(uint2));
    initSupportPixels(h_supportPixelsUsed);
    iostream.memcpyHtoDAsync(d_supportPixelsUsed, h_supportPixelsUsed);

    // Initialize frequencies
    float frequencies[CHANNELS];
    initFrequencies(frequencies);

    // Initialize visibilities
    HostMemory h_visibilities(sizeof(wpg::VisibilitiesType));
    initVisibilities(h_visibilities);

    // Initialize uvw
    std::cout << ">> Initialize uvw" << std::endl;
    HostMemory h_uvw(BLOCKS * sizeof(wpg::UVWType));
    wpg::WPlanesType *w_planes = (wpg::WPlanesType *) malloc(sizeof(wpg::WPlanesType));
    int nr_w_planes_max;
    initUVW(h_uvw, (void *) w_planes, &nr_w_planes_max, NR_STATIONS, BASELINES, NR_TIMESTEPS, NR_TIMESLOTS, CHANNELS, INTEGRATION_TIME, GRID_U, IMAGE_SIZE, frequencies);
    std::cout << " nr_w_planes: " << nr_w_planes_max << std::endl << std::endl;

    // Performance measurement
    double runtime;

    #if defined(HAVE_POWERSENSOR)
    const char* sensor_device_cstr = getenv("SENSOR_DEVICE");
    powersensor::PowerSensor *powersensor_device;
    powersensor::PowerSensor *powersensor_host;

    if (sensor_device_cstr) {
        powersensor_device = powersensor::arduino::ArduinoPowerSensor::create(sensor_device_cstr);
    } else {
        powersensor_device = powersensor::nvml::NVMLPowerSensor::create(device);
    }

    powersensor_host = powersensor::likwid::LikwidPowerSensor::create();

    powersensor::State power_states[4];
    #endif

    #pragma omp parallel num_threads(STREAMS)
    {
        checkCudaCall(cudaSetDevice(device));
        checkCudaCall(cudaGetLastError());

        Event  startCopy, finishedCopy, startCompute, finishedCompute;

        #pragma omp single
        runtime = -omp_get_wtime();

        #if defined(HAVE_POWERSENSOR)
        #pragma omp single
        {
            power_states[0] = powersensor_device->read();
            power_states[1] = powersensor_host->read();
        }
        #endif

#if MODE == MODE_ATERM
        /*
         * AW-Projection
         */

        omp_set_nested(true);

        // Determine jobisze based on the amount of device memory
        uint64_t sizeof_device_memory = get_free_device_memory();
        uint64_t sizeof_support = nr_w_planes_max * sizeof(wpg::SupportType);
        unsigned jobsize = (0.8 * sizeof_device_memory) / (sizeof_support * STREAMS);
        jobsize = std::min(jobsize, (unsigned) AWPROJECTION_JOBSIZE);

        // Allocate host support type for multiple baselines
        typedef float2 FullSupportType[jobsize][nr_w_planes_max][SUPPORT_V][OVERSAMPLE_V][SUPPORT_U][OVERSAMPLE_U];
        HostMemory h_support(sizeof(FullSupportType));
        DeviceMemory d_support(sizeof(FullSupportType));

        // Allocate device memory for visibilities and uvw coordinates for one timeslot
        size_t visibilities_size = (sizeof(wpg::VisibilitiesType) / BASELINES) * jobsize;
        size_t uvw_size = (sizeof(wpg::UVWType) / BASELINES) * jobsize;
        DeviceMemory d_visibilities(visibilities_size);
        DeviceMemory d_uvw(uvw_size);

        // Create FFT Plan
        #if AWPROJECTION_FFT_KERNEL
        cufft::C2C_2D *plan;
        #pragma omp critical
        {
            plan = new cufft::C2C_2D(
                SUPPORT_V * OVERSAMPLE_V,
                SUPPORT_U * OVERSAMPLE_U,
                1,
                SUPPORT_V * OVERSAMPLE_V * SUPPORT_U * OVERSAMPLE_U,
                nr_w_planes_max);
            plan->setStream(executestream);
        }
        #endif

        // Iterate all baselines in jobs
        #pragma omp for
        for (unsigned bl = 0; bl < BASELINES; bl += jobsize) {
            jobsize = bl + jobsize > BASELINES ? BASELINES - bl : jobsize;

            // Update size of input data to copy for current job
            visibilities_size = (sizeof(wpg::VisibilitiesType) / BASELINES) * jobsize;
            uvw_size = (sizeof(wpg::UVWType) / BASELINES) * jobsize;

            // Iterate all timeslots
            for (unsigned block = 0; block < BLOCKS; block++) {
                // Get number of w-planes for current block
                int nr_w_planes = (*w_planes)[bl][block];

                // Offset in full uvw datastructure for current timeslot
                size_t uvw_offset = block * sizeof(wpg::UVWType);

                // Offset in full visibilities datastructure for current job
                size_t visibilities_offset = bl * TIMESTEPS * CHANNELS * POLARIZATIONS * sizeof(float2);

                // Initialize kernel
                #if AWPROJECTION_INIT_KERNEL
                #pragma omp parallel for
                for (int i = 0; i < jobsize; i++) {
                    // Get number of w-planes for current block
                    int nr_w_planes = (*w_planes)[i][block];

                    // Initialize support
                    initSupport(h_support, nr_w_planes);
                }
                #endif

                #pragma omp critical(GPU)
                {
                    // Copy data to device
                    startCopy.record(iostream);
                    iostream.memcpyHtoDAsync(d_visibilities, h_visibilities, visibilities_size, visibilities_offset);
                    iostream.memcpyHtoDAsync(d_uvw, h_uvw, uvw_size, uvw_offset);
                    #if AWPROJECTION_COPY_KERNEL
                    auto sizeof_support = nr_w_planes * sizeof(wpg::SupportType);
                    iostream.memcpyHtoDAsync(d_support, h_support, sizeof_support);
                    #endif
                    finishedCopy.record(iostream);

                    // Wait for data to be copied
                    executestream.wait(finishedCopy);
                    executestream.record(startCompute);

                    // Perform 2D fft for every baseline and w-plane
                    #if AWPROJECTION_FFT_KERNEL
                    auto offset = (unsigned long) nr_w_planes_max * SUPPORT_V * OVERSAMPLE_V * SUPPORT_U * OVERSAMPLE_U;
                    for (int i = 0; i < jobsize; i++) {
                        cufftComplex *data_ptr = (cufftComplex *) ((wpg::float2 *) d_support + i * offset);
                        plan->execute(data_ptr, data_ptr, CUFFT_FORWARD);
                    }
                    #endif

                    // Perform aw-projection
                    wprojection<<<jobsize, NR_THREADS_PER_BLOCK, 0, executestream>>>(d_grid, d_support, d_visibilities, d_uvw, d_supportPixelsUsed);
                    executestream.record(finishedCompute);
                }

                finishedCompute.synchronize();
                checkCudaCall(cudaGetLastError());

            } // end for block
        } // end for bl
#else

        /*
         * W-Projection
         */
        DeviceMemory d_visibilities(sizeof(wpg::VisibilitiesType));
        DeviceMemory d_uvw(sizeof(wpg::UVWType));

        // Initialize support
        HostMemory h_support(nr_w_planes_max * sizeof(wpg::SupportType));
        DeviceMemory d_support(nr_w_planes_max * sizeof(wpg::SupportType));
        initSupport(h_support, nr_w_planes_max);
        iostream.memcpyHtoDAsync(d_support, h_support);

        #pragma omp for
        for (unsigned block = 0; block < BLOCKS; block++) {
            // Size of uvw coordinates for one timeslot
            size_t uvw_size  = sizeof(wpg::UVWType);

            // Offset in full uvw datastructure for current timeslot
            size_t uvw_offset = block * sizeof(wpg::UVWType);

            #pragma omp critical(GPU)
            {
                startCopy.record(iostream);
                iostream.memcpyHtoDAsync(d_visibilities, h_visibilities);
                iostream.memcpyHtoDAsync(d_uvw, h_uvw, uvw_size, uvw_offset);
                finishedCopy.record(iostream);

                executestream.wait(finishedCopy);
                executestream.record(startCompute);
                wprojection<<<BASELINES, NR_THREADS_PER_BLOCK, 0, executestream>>>(d_grid, d_support, d_visibilities, d_uvw, d_supportPixelsUsed);
                executestream.record(finishedCompute);
            }

            finishedCompute.synchronize();
            checkCudaCall(cudaGetLastError());
        } // end for block
#endif

        #if defined(HAVE_POWERSENSOR)
        #pragma omp single
        {
            power_states[2] = powersensor_device->read();
            power_states[3] = powersensor_host->read();
        }
        #endif
    } // end for parallel

    // Wait for all threads to finish
    checkCudaCall(cudaThreadSynchronize());

    // Copy grid to host
    Event startCopy, finishedCopy;
    startCopy.record();
    iostream.memcpyDtoHAsync(h_grid, d_grid);
    finishedCopy.record();
    finishedCopy.synchronize();

    // Print performance
    runtime += omp_get_wtime();
    std::cout << ">> Results" << std::endl;
    print_throughput(runtime);

    // Print power consumption
    #if defined(HAVE_POWERSENSOR)
    print_power("device: ", powersensor::PowerSensor::Joules(power_states[0], power_states[2]));
    print_power(" host: ",  powersensor::PowerSensor::Joules(power_states[1], power_states[3]));
    #endif

    // Plot grid
    plotGrid(h_grid, GRID_U, GRID_V);

    // Print grid
    #if defined(DEBUG)
    printGrid(h_grid, GRID_U, GRID_V);
    #endif

    #if defined(HAVE_POWERSENSOR)
    delete powersensor_device;
    delete powersensor_host;
    #endif
}

int main() {
    std::cout << ">> Parameters" << std::endl;
    print_parameters();

    #if 0
    printDeviceProperties();
    #endif

    int deviceCount = 0;
    checkCudaCall(cudaGetDeviceCount(&deviceCount));

    if (deviceCount < 1) {
        fprintf(stderr, "No CUDA devices found.\n");
        exit(EXIT_FAILURE);
    }

    doCuda();

    return EXIT_SUCCESS;
}
