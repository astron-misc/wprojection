#include <complex>
#include <omp.h>

#include "init.h"
#include "types.h"

using namespace wpg;

inline float meters_to_lambda(float meters, float frequency) {
    const double speed_of_light = 299792458.0;
    return meters * (frequency / speed_of_light);
}

inline float meters_to_pixels(float meters, float imagesize, float frequency) {
    return imagesize * meters_to_lambda(meters, frequency);
}

void initUVW(
    void *uvw_ptr,
    void *w_planes_ptr,
    int *w_planes_max,
    int nr_stations,
    int nr_baselines,
    int nr_timesteps,
    int nr_timeslots,
    int nr_channels,
    float integration_time,
    int grid_size,
    float image_size,
    const float *frequencies)
{
    int nr_timesteps_timeslot = nr_timesteps / nr_timeslots;

    typedef struct { float u, v, w; } UVW;
    typedef UVW UVWType[nr_baselines][nr_timesteps][nr_channels];
    typedef unsigned WPlanesType[nr_baselines][nr_timeslots];

    UVWType *uvw = (UVWType *) uvw_ptr;
    WPlanesType *w_planes = (WPlanesType *) w_planes_ptr;

    // Check whether layout file exists
    char filename[512];
    sprintf(filename, "%s/%s", LAYOUT_DIR, LAYOUT_FILE);

    // Read the number of stations in the layout file.
    int nr_stations_file = uvwsim_get_num_stations(filename);

    // Allocate memory for antenna coordinates
    double *x = (double*) malloc(nr_stations_file * sizeof(double));
    double *y = (double*) malloc(nr_stations_file * sizeof(double));
    double *z = (double*) malloc(nr_stations_file * sizeof(double));

    // Load the antenna coordinates
    #if defined(DEBUG)
    printf("looking for stations file in: %s\n", filename);
    #endif

    if (uvwsim_load_station_coords(filename, nr_stations_file, x, y, z) != nr_stations_file) {
        std::cerr << "Failed to read antenna coordinates." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Select some antennas randomly when not all antennas are requested
    if (nr_stations < nr_stations_file) {
        // Allocate memory for selection of antenna coordinates
        double *_x = (double*) malloc(nr_stations * sizeof(double));
        double *_y = (double*) malloc(nr_stations * sizeof(double));
        double *_z = (double*) malloc(nr_stations * sizeof(double));

        // Generate nr_stations random numbers
        int station_number[nr_stations];
        int i = 0;
        srandom(RANDOM_SEED);
        while (i < nr_stations) {
            int index = nr_stations_file * ((double) random() / RAND_MAX);
            bool found = true;
            for (int j = 0; j < i; j++) {
                if (station_number[j] == index) {
                    found = false;
                    break;
                }
            }
            if (found) {
                 station_number[i++] = index;
            }
         }

        // Set stations
        for (int i = 0; i < nr_stations; i++) {
            _x[i] = x[station_number[i]];
            _y[i] = y[station_number[i]];
            _z[i] = z[station_number[i]];
        }

        // Swap pointers and free memory
        double *__x = x;
        double *__y = y;
        double *__z = z;
        x = _x;
        y = _y;
        z = _z;
        free(__x);
        free(__y);
        free(__z);
    }

    // Define observation parameters
    double ra0  = RIGHT_ASCENSION;
    double dec0 = DECLINATION;
    double start_time_mjd = uvwsim_datetime_to_mjd(YEAR, MONTH, DAY, HOUR, MINUTE, SECONDS);
    double obs_length_hours = (nr_timesteps * integration_time) / (3600.0);
    double obs_length_days = obs_length_hours / 24.0;

    // Allocate memory for baseline coordinates
    int nr_coordinates = nr_timesteps * nr_baselines;
    double *uu = (double*) malloc(nr_coordinates * sizeof(double));
    double *vv = (double*) malloc(nr_coordinates * sizeof(double));
    double *ww = (double*) malloc(nr_coordinates * sizeof(double));

    // Evaluate baseline uvw coordinates.
    #pragma omp parallel for
    for (int t = 0; t < nr_timesteps; t++) {
        double time_mjd = start_time_mjd + t
                          * (obs_length_days/(double)nr_timesteps);
        size_t offset = t * nr_baselines;
        uvwsim_evaluate_baseline_uvw(
            &uu[offset], &vv[offset], &ww[offset],
            nr_stations, x, y, z, ra0, dec0, time_mjd);
    }

    // W step size in lambda
    float w_step = 1 / ((float) IMAGE_SIZE * IMAGE_SIZE);

    float global_u_min = 0;
    float global_u_max = 0;
    float global_v_min = 0;
    float global_v_max = 0;
    int   global_w_min = 0;
    int   global_w_max = 0;

    // Determine number of threads
    int nr_threads;
    #pragma omp parallel
    {
        nr_threads = omp_get_num_threads();
    }

    // Initialize min an max variables for every thread
    float u_min[nr_threads];
    float u_max[nr_threads];
    float v_min[nr_threads];
    float v_max[nr_threads];
    float w_min[nr_threads];
    float w_max[nr_threads];
    for (unsigned thread_id = 0; thread_id < nr_threads; thread_id++) {
        u_min[thread_id] = 0;
        u_max[thread_id] = 0;
        v_min[thread_id] = 0;
        v_max[thread_id] = 0;
        w_min[thread_id] = 0;
        w_max[thread_id] = 0;
    }

    // Fill uvw datastructure
    #pragma omp parallel for
    for (int bl = 0; bl < nr_baselines; bl++) {
        unsigned thread_id = omp_get_thread_num();

        for (int ts = 0; ts < nr_timeslots; ts++) {
            float current_w_min = 0;
            float current_w_max = 0;
            for (int t = 0; t < nr_timesteps_timeslot; t++) {
                for (int c = 0; c < nr_channels; c++) {
                    int i = ((ts * nr_timesteps_timeslot) + t) * nr_baselines + bl;
                    float u_meters = uu[i];
                    float v_meters = vv[i];
                    float w_meters = ww[i];
                    float u_pixels = meters_to_pixels(u_meters, image_size, frequencies[c]) + (grid_size / 2);
                    float v_pixels = meters_to_pixels(v_meters, image_size, frequencies[c]) + (grid_size / 2);
                    float w_index = meters_to_lambda(w_meters, frequencies[c]) / w_step;
                    UVW value = {u_pixels, v_pixels, w_index};
                    (*uvw)[bl][ts * nr_timesteps_timeslot + t][c] = value;

                    #if defined(DEBUG)
                    if (u_pixels > u_max[thread_id]) u_max[thread_id] = u_pixels;
                    if (v_pixels > v_max[thread_id]) v_max[thread_id] = v_pixels;
                    if (u_pixels < u_min[thread_id]) u_min[thread_id] = u_pixels;
                    if (v_pixels < v_min[thread_id]) v_min[thread_id] = v_pixels;
                    #endif
                    if (w_index > current_w_max) current_w_max = w_index;
                    if (w_index < current_w_min) current_w_min = w_index;
                }
            }

            // Compute the amount of w planes needed for the current timeslot
            int nr_w_planes = (int) ceilf(current_w_max - current_w_min);
            (*w_planes)[bl][ts] = nr_w_planes;
            if (nr_w_planes > w_max[thread_id]) {
                w_max[thread_id] = nr_w_planes;
            }

            // Shift w_index down
            for (int i = 0; i < nr_timesteps_timeslot * nr_channels; i++) {
                UVW& value = (*uvw)[bl][ts * nr_timesteps_timeslot][i];
                value.w -= current_w_min;
            }
        } // end for timeslots
    } // end for baselines

    // Determine smallest and largest values for all baselines and timeslots
    for (unsigned thread_id = 0; thread_id < nr_threads; thread_id++) {
        if (u_max[thread_id] > global_u_max) global_u_max = u_max[thread_id];
        if (v_max[thread_id] > global_v_max) global_v_max = v_max[thread_id];
        if (w_max[thread_id] > global_w_max) global_w_max = w_max[thread_id];
        if (u_min[thread_id] < global_u_min) global_u_min = u_min[thread_id];
        if (v_min[thread_id] < global_v_min) global_v_min = v_min[thread_id];
        if (w_min[thread_id] < global_w_min) global_w_min = w_min[thread_id];
    }

    // Make all w values positive
    if (global_w_min < 0) {
        global_w_max += global_w_min;
        #pragma omp parallel for
        for (int i = 0; i < nr_coordinates; i++) {
            ww[i] += global_w_min;
        }
    }

    // Debugging
    #if defined(DEBUG)
    printf("u: %.2f,%.2f\n", global_u_min, global_u_max);
    printf("v: %.2f,%.2f\n", global_v_min, global_v_max);
    printf("w: %.2f,%.2f\n", global_w_min, global_w_max);
    #endif

    // Set the maximum number of w-planes for any baseline or timeslot
    *w_planes_max = global_w_max;

    // Free memory
    free(x); free(y); free(z);
    free(uu); free(vv); free(ww);
}

void initSupport(void *ptr, int nr_w_planes) {

    SupportType *support = (SupportType *) ptr;

#if MODE == MODE_SIMPLE
	for (int v = 0; v < SUPPORT_V; v ++) {
		for (int u = 0; u < SUPPORT_U; u ++) {
			(*support)[v][u] = {(float) std::min(v + 1, SUPPORT_V - v) * std::min(u + 1, SUPPORT_U - u), 0};
		}
	}
#elif MODE == MODE_OVERSAMPLE
#if ORDER == ORDER_W_OV_OU_V_U
	for (int w = 0; w < nr_w_planes; w ++) {
		for (int ou = 0; ou < OVERSAMPLE_U; ou ++) {
			for (int ov = 0; ov < OVERSAMPLE_V; ov ++) {
				for (int v = 0; v < SUPPORT_V; v ++) {
					for (int u = 0; u < SUPPORT_U; u ++) {
						(*support)[w][ov][ou][v][u] = {
						(w + 1) * (float) std::min(v + 1, SUPPORT_V - v) * std::min(u + 1, SUPPORT_U - u),
						(w + 1) * (float) (ov + 1) * (ou + 1)};
					}
				}
			}
		}
	}
#elif ORDER == ORDER_W_V_OV_U_OU
	for (int w = 0; w < nr_w_planes; w ++) {
		for (int v = 0; v < SUPPORT_V; v ++) {
			for (int ov = 0; ov < OVERSAMPLE_V; ov ++) {
				for (int u = 0; u < SUPPORT_U; u ++) {
					for (int ou = 0; ou < OVERSAMPLE_U; ou ++) {
						(*support)[w][v][ov][u][ou] = {
						(w + 1) * (float) std::min(v + 1, SUPPORT_V - v) * std::min(u + 1, SUPPORT_U - u),
						(w + 1) * (float) (ov + 1) * (ou + 1)};
					}
				}
			}
		}
	}
#endif
#elif MODE == MODE_ATERM
	for (int w = 0; w < nr_w_planes; w ++) {
	    for (int v = 0; v < SUPPORT_V; v ++) {
            for (int ov = 0; ov < OVERSAMPLE_V; ov ++) {
                for (int u = 0; u < SUPPORT_U; u ++) {
                    for (int ou = 0; ou < OVERSAMPLE_U; ou ++) {
                        (*support)[w][v][ov][u][ou] =  {
                        (float) std::min(v + 1, SUPPORT_V - v) * std::min(u + 1, SUPPORT_U - u),
                        (float) (ov + 1) * (ou + 1)};
                    }
                }
            }
	    }
    }
#endif
}


void initFrequencies(float frequencies[CHANNELS]) {
	for (unsigned ch = 0; ch < CHANNELS; ch ++) {
        frequencies[ch] = START_FREQUENCY + FREQUENCY_INCREMENT * ch;
	}
}

void initVisibilities(void *ptr) {
    typedef std::complex<float> VisibilitiesType[BASELINES][TIMESTEPS][CHANNELS][POLARIZATIONS];
    VisibilitiesType *visibilities = (VisibilitiesType *) ptr;

    std::complex<float> vis = {1.0f, 0.0f};

	#pragma omp parallel for
	for (unsigned i = 0; i < BASELINES * TIMESTEPS * CHANNELS * POLARIZATIONS; i ++) {
		(*visibilities)[0][0][0][i] = vis;
	}
}

void initSupportPixels(void *ptr) {
    typedef struct { unsigned x, y; } uint2;
    uint2 *supportPixelsUsed = (uint2 *) ptr;

    for (unsigned bl = 0; bl < BASELINES; bl ++) {
    	supportPixelsUsed[bl].x = X;
    	supportPixelsUsed[bl].y = X;
    }
}
