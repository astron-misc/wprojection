#include "kernels.h"

/******************************************************************************/
//	addGrids
/******************************************************************************/
void addGrids(
    GridType a,
    const GridType b[],
    const unsigned nrThreads)
{
    #pragma omp parallel for
    for (int v = 0; v < GRID_V; v ++) {
    	for (unsigned u = 0; u < GRID_U; u ++) {
    		for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
    			float2 sum = b[0][v][u][pol];

    			for (unsigned g = 1; g < nrThreads; g ++) {
    				sum += b[g][v][u][pol];
    			}

    			a[v][u][pol] += sum;
    		}
    	}
    }
}



/******************************************************************************/
//	wprojection
/******************************************************************************/
void wprojection(
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
	#pragma omp parallel
	{
		GridType *localGrid = &grid[omp_get_thread_num()];

		#if MODE == MODE_OVERSAMPLE
		#pragma omp for schedule(dynamic)
		for (int bl = 0; bl < BASELINES; bl ++) {
			unsigned v_end  = supportPixelsUsed[bl].y;

			for (unsigned v = 0; v < v_end; v ++) {
				for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
					float uc = uvw[bl][0][ch].x;
					float vc = uvw[bl][0][ch].y;
					unsigned wc = (unsigned) uvw[bl][0][ch].z;
					unsigned grid_u = (unsigned) uc;
					unsigned grid_v = (unsigned) vc;
					float	   u_frac = uc - grid_u;
					float	   v_frac = vc - grid_v;
					unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
					unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
					unsigned u_end  = supportPixelsUsed[bl].x;

					float2 visXX = visibilities[bl][0][ch][0];
					float2 visXY = visibilities[bl][0][ch][1];
					float2 visYX = visibilities[bl][0][ch][2];
					float2 visYY = visibilities[bl][0][ch][3];

					for (unsigned u = 0; u < u_end; u ++) {
						#if ORDER == ORDER_W_OV_OU_V_U
						float2 weight = support[wc][ov][ou][v][u];
						#elif ORDER == ORDER_W_V_OV_U_OU
						float2 weight = support[wc][v][ov][u][ou];
						#endif
						(*localGrid)[grid_v + v][grid_u + u][0] += visXX * weight;
						(*localGrid)[grid_v + v][grid_u + u][1] += visXY * weight;
						(*localGrid)[grid_v + v][grid_u + u][2] += visYX * weight;
						(*localGrid)[grid_v + v][grid_u + u][3] += visYY * weight;
					}
				}
			}
		}

		#elif MODE == MODE_SIMPLE
		#pragma omp for schedule(dynamic)
		for (int bl = 0; bl < BASELINES; bl ++) {
			unsigned u_end  = supportPixelsUsed[bl].x;
			unsigned v_end  = supportPixelsUsed[bl].y;

			for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
				unsigned grid_u = (unsigned) nearbyintf(uvw[bl][0][ch].x);
				unsigned grid_v = (unsigned) nearbyintf(uvw[bl][0][ch].y);

				for (unsigned v = 0; v < v_end; v ++) {
					for (unsigned u = 0; u < u_end; u ++) {
						float2 weight = support[v][u];

						for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
							float2 prod   = visibilities[bl][0][ch][pol] * weight;
							float2 &pixel = (*localGrid)[grid_v + v][grid_u + u][pol];
							pixel += prod;
						}
					}
				}
			}
		}
		#endif
	}
}


/******************************************************************************/
//	awprojection
/******************************************************************************/
#if MODE == MODE_ATERM
void awprojection(
    const unsigned bl,
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
    unsigned v_end  = supportPixelsUsed[bl].y;

    GridType *localGrid = &grid[omp_get_thread_num()];

    for (unsigned v = 0; v < v_end; v ++) {
    	for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
    		float uc = uvw[bl][0][ch].x;
    		float vc = uvw[bl][0][ch].y;
            unsigned wc = (unsigned) uvw[bl][0][ch].z;
    		unsigned grid_u = (unsigned) uc;
    		unsigned grid_v = (unsigned) vc;
    		float	   u_frac = uc - grid_u;
    		float	   v_frac = vc - grid_v;
    		unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
    		unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
    		unsigned u_end  = supportPixelsUsed[bl].x;

    		float2 visXX = visibilities[bl][0][ch][0];
    		float2 visXY = visibilities[bl][0][ch][1];
    		float2 visYX = visibilities[bl][0][ch][2];
    		float2 visYY = visibilities[bl][0][ch][3];

    		for (unsigned u = 0; u < u_end; u ++) {
                float2 weight = support[wc][v][ov][u][ou];
    			(*localGrid)[grid_v + v][grid_u + u][0] += visXX * weight;
    			(*localGrid)[grid_v + v][grid_u + u][1] += visXY * weight;
    			(*localGrid)[grid_v + v][grid_u + u][2] += visYX * weight;
    			(*localGrid)[grid_v + v][grid_u + u][3] += visYY * weight;
    		}
    	}
    }
}
#endif
