#include <iostream>

#include <cuda_runtime.h>

inline void checkCudaCallWithLineNumber(
    cudaError_t result,
    char const *const func,
    const char *const file,
    int const line)
{
  if (result != cudaSuccess) {
    #pragma omp critical
    std::cerr << "CUDA Error at " << file;
    std::cerr << ":" << line;
    std::cerr << " in function " << func;
    std::cerr << ": " << cudaGetErrorString(result);
    std::cerr << std::endl;
    exit(1);
  }
}

#define checkCudaCall(val) checkCudaCallWithLineNumber((val), #val, __FILE__, __LINE__)

inline void printDeviceProperties()
{
  int deviceCount;
  cudaGetDeviceCount(&deviceCount);
  std::cout << "deviceCount = " << deviceCount << std::endl;

  for (int device = 0; device < deviceCount; device++) {
    cudaDeviceProp p;
    cudaGetDeviceProperties(&p, device);

    std::cout << "device " << device << std::endl
	      << "  name: " << p.name << std::endl
	      << "  global mem: " << p.totalGlobalMem << std::endl
	      << "  sharedMemPerBlock: " << p.sharedMemPerBlock << std::endl
	      << "  regsPerBlock: " << p.regsPerBlock << std::endl
	      << "  warpSize: " << p.warpSize << std::endl
	      << "  memPitch: " << p.memPitch << std::endl
	      << "  maxThreadsPerBlock: " << p.maxThreadsPerBlock << std::endl
	      << "  maxThreadsPerMultiProcessor: " << p.maxThreadsPerMultiProcessor << std::endl
	      << "  maxBlockSize: (" << p.maxThreadsDim[0] << ", "<< p.maxThreadsDim[1] << ", "<< p.maxThreadsDim[2] << ")" << std::endl
	      << "  maxGridSize: (" << p.maxGridSize[0] << ", "<< p.maxGridSize[1] << ", "<< p.maxGridSize[2] << ")" << std::endl
	      << "  const mem: " << p.totalConstMem << std::endl
	      << "  version: " << p.major << "." << p.minor << std::endl
	      << "  clock rate: " << p.clockRate << std::endl
	      << "  tex alignment: " << p.textureAlignment << std::endl
	      << "  multiprocessors: " << p.multiProcessorCount << std::endl;
  }
}

inline size_t get_free_device_memory() {
    size_t free;
    size_t total;
    checkCudaCall(cudaMemGetInfo(&free, &total));
    return free;
}

class Event;

class Memory  {
    public:
        size_t size() {
            return _size;
        }

        operator void* () {
            return _ptr;
        }

        template <typename T> operator T() {
            return (T) _ptr;
        }

    protected:
        void *_ptr;
        size_t _size;
};

class DeviceMemory : public Memory {
    public:
        DeviceMemory(size_t size) {
            _size = size;
            checkCudaCall(cudaMalloc(&_ptr, size));
        }

        ~DeviceMemory() {
            checkCudaCall(cudaFree(_ptr));
        };
};

class HostMemory : public Memory {
    public:
        HostMemory(size_t size) {
            _size = size;
            checkCudaCall(cudaHostAlloc(&_ptr, size, 0));
        }

        ~HostMemory() {
            checkCudaCall(cudaFreeHost(_ptr));
        };
};

class Stream
{
  public:
    Stream()
    {
        checkCudaCall(cudaStreamCreate(&stream));
    }

    ~Stream()
    {
        checkCudaCall(cudaStreamDestroy(stream));
    }

    void synchronize()
    {
        checkCudaCall(cudaStreamSynchronize(stream));
    }

    void wait(Event &event, unsigned int flags = 0)
    {
        checkCudaCall(cudaStreamWaitEvent(stream, (cudaEvent_t &) event, flags));
    }

    void record(Event &event)
    {
        checkCudaCall(cudaEventRecord ((cudaEvent_t &) event, stream));
    }

    void memcpyHtoDAsync(DeviceMemory &devPtr, HostMemory &hostPtr, size_t size = 0, size_t hostOffset = 0) {
        void *src = (void *) ((size_t) hostPtr + hostOffset);
        if (size > 0) {
            checkCudaCall(cudaMemcpyAsync(devPtr, src, size, cudaMemcpyHostToDevice, stream));
        } else {
            checkCudaCall(cudaMemcpyAsync(devPtr, src, devPtr.size(), cudaMemcpyHostToDevice, stream));
        }
    }

    void memcpyDtoHAsync(HostMemory &hostPtr, DeviceMemory &devPtr, size_t size = 0) {
        if (size > 0) {
            checkCudaCall(cudaMemcpyAsync(hostPtr, devPtr, size, cudaMemcpyDeviceToHost, stream));
        } else {
            checkCudaCall(cudaMemcpyAsync(hostPtr, devPtr, devPtr.size(), cudaMemcpyDeviceToHost, stream));
        }
    }

    operator cudaStream_t & ()
    {
      return stream;
    }

  private:
    cudaStream_t stream;
};


class Event
{
  public:
    Event()
    {
      checkCudaCall(cudaEventCreate(&event));
    }

    ~Event()
    {
      checkCudaCall(cudaEventDestroy(event));
    }

    void record()
    {
      checkCudaCall(cudaEventRecord(event));
    }

    void record(Stream &stream)
    {
      checkCudaCall(cudaEventRecord(event, stream));
    }

    void synchronize()
    {
      checkCudaCall(cudaEventSynchronize(event));
    }

    float elapsedTime(Event &start)
    {
      float time;

      checkCudaCall(cudaEventElapsedTime(&time, start.event, this->event));
      return time;
    }

    operator cudaEvent_t & ()
    {
      return event;
    }

  private:
    cudaEvent_t event;
};
