#include <iostream>
#include <complex>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "config.h"
#include "defines.h"
#include "uvwsim.h"

/* Constants */
#define RANDOM_SEED         1234
#ifndef SPEED_OF_LIGHT
#define SPEED_OF_LIGHT      299792458.0
#endif

/* Observation parameters */
static const std::string ENV_LAYOUT_FILE;
#define LAYOUT_FILE         "SKA1_low_ecef"
#define START_FREQUENCY     150e6
#define FREQUENCY_INCREMENT 0.7e6
#define RIGHT_ASCENSION     (10.0 * (M_PI/180.))
#define DECLINATION         (70.0 * (M_PI/180.))
#define YEAR                2014
#define MONTH               03
#define DAY                 20
#define HOUR                01
#define MINUTE              57
#define SECONDS             1.3
#define INTEGRATION_TIME    1.0f

/* Methods */
void initUVW(
    void *uvw_ptr,
    void *w_planes_ptr, // array containing number of w_planes per baseline
    int *w_planes_max, // integer pointer to the maximum number of w_planes for any baseline
    int nr_stations,
    int nr_baselines,
    int nr_timesteps,
    int nr_timeslots,
    int nr_channels,
    float integration_time,
    int grid_size,
    float image_size,
    const float *frequencies);


void initSupport(void *ptr, int nr_w_planes);

void initFrequencies(float *ptr);

void initVisibilities(void *ptr);

void initSupportPixels(void *ptr);
