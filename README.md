# Introduction
This repository contains W-Projection gridder kernels, based on the original code by John Romein and refactored/updated by Bram Veenboer.

# Quick build/run guide:
mkdir build  
cd build  
cmake  ..  
make  
make install  