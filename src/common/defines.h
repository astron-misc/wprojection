#include "config.h"

#define MODE_SIMPLE	     0
#define MODE_OVERSAMPLE	 1
#define MODE_ATERM       2

#if defined(ENABLE_ATERM)
#define MODE		 MODE_ATERM
#else
#define MODE         MODE_OVERSAMPLE
#endif
#define OVERSAMPLE_U 8
#define OVERSAMPLE_V 8

#define SUPPORT_U   SUPPORT_SIZE
#define SUPPORT_V   SUPPORT_SIZE

#define BASELINES	(NR_STATIONS * (NR_STATIONS - 1) / 2)
#define BLOCKS		NR_TIMESLOTS
#define TIMESTEPS   NR_TIMESTEPS/NR_TIMESLOTS
#define CHANNELS	NR_CHANNELS
#define GRID_U      GRID_SIZE
#define GRID_V      GRID_SIZE
#define	STREAMS		2

#define POLARIZATIONS   4
#define SPEED_OF_LIGHT	299792458.
#define X               SUPPORT_U

#define ORDER			ORDER_W_OV_OU_V_U

#define ORDER_W_OV_OU_V_U	0
#define ORDER_W_V_OV_U_OU	1
