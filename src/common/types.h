namespace wpg {
    /* Datatypes */
    typedef struct {
        float x, y;
        operator std::complex<float>() const {
            return std::complex<float>(x, y);
        }
    } float2;

    typedef struct { float x, y, z; } float3;

    typedef struct {unsigned x, y; } uint2;

    /* Datastructures */
    typedef float2 GridType[GRID_V][GRID_U][POLARIZATIONS];
    typedef float3 UVWType[BASELINES][TIMESTEPS][CHANNELS];
    typedef float2 VisibilitiesType[BASELINES][TIMESTEPS][CHANNELS][POLARIZATIONS];
    typedef unsigned WPlanesType[BASELINES][NR_TIMESLOTS];

    #if MODE == MODE_SIMPLE
    typedef float2 SupportType[SUPPORT_V][SUPPORT_U];
    #elif MODE == MODE_OVERSAMPLE
    #if ORDER == ORDER_W_OV_OU_V_U
    typedef float2 SupportType[1][OVERSAMPLE_V][OVERSAMPLE_U][SUPPORT_V][SUPPORT_U];
    #elif ORDER == ORDER_W_V_OV_U_OU
    typedef float2 SupportType[1][SUPPORT_V][OVERSAMPLE_V][SUPPORT_U][OVERSAMPLE_U];
    #endif
    #elif MODE == MODE_ATERM
    typedef float2 SupportType[1][SUPPORT_V][OVERSAMPLE_V][SUPPORT_U][OVERSAMPLE_U];
    #endif

    /* Operators */
    inline float2 make_float2(float x, float y)
    {
        float2 f = { x, y }; return f;
    }

    inline float2 operator += (float2 &a, float2 b)
    {
      return make_float2(a.x += b.x, a.y += b.y);
    }

    inline float2 operator * (float2 a, float2 b)
    {
      return make_float2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
    }
}
