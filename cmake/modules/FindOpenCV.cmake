# - Try to find OpenCV library installation
#
# The follwoing variables are optionally searched for defaults
#  OPENCV_ROOT_DIR:            Base directory of OpenCv tree to use.
#  OPENCV_FIND_REQUIRED_COMPONENTS : FIND_PACKAGE(OpenCV COMPONENTS ..)
#    compatible interface. typically  CV CXCORE CVAUX HIGHGUI CVCAM .. etc.
#
# The following are set after configuration is done:
#  OPENCV_FOUND
#  OPENCV_INCLUDE_DIR
#  OPENCV_LIBRARIES

find_package(PackageHandleStandardArgs)

# Required cv components with header and library
SET(OpenCV_FIND_REQUIRED_COMPONENTS CORE HIGHGUI IMGPROC)


# Typical root dirs of installation
SET(OpenCV_POSSIBLE_ROOT_DIRS
    ${OPENCV_ROOT_DIR}
    $ENV{OPENCV_ROOT_DIR}
)

# OpenCV root directory
FIND_PATH(OpenCV_ROOT_DIR
    NAMES
    include/opencv2/opencv.hpp
    PATHS ${OpenCV_POSSIBLE_ROOT_DIRS}
)

# Header include dir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_INCDIR_SUFFIXES
    include/opencv2
)

# Library dir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_LIBDIR_SUFFIXES
    lib
)

# Find include dir for each library
FIND_PATH(OpenCV_HIGHGUI_INCLUDE_DIR
    NAMES highgui.hpp
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES})
FIND_PATH(OpenCV_CORE_INCLUDE_DIR
    NAMES core.hpp
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES})
FIND_PATH(OpenCV_IMGPROC_INCLUDE_DIR
    NAMES imgproc.hpp
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES})


# Find sbsolute path to all libraries
FIND_LIBRARY(OpenCV_CORE_LIBRARY
    NAMES opencv_core
    PATHS ${OpenCV_ROOT_DIR} PATH_SUFFIXES ${OpenCV_LIBDIR_SUFFIXES})
FIND_LIBRARY(OpenCV_HIGHGUI_LIBRARY
    NAMES opencv_highgui
    PATHS ${OpenCV_ROOT_DIR} PATH_SUFFIXES ${OpenCV_LIBDIR_SUFFIXES})
FIND_LIBRARY(OpenCV_IMGPROC_LIBRARY
    NAMES opencv_imgproc
    PATHS ${OpenCV_ROOT_DIR} PATH_SUFFIXES ${OpenCV_LIBDIR_SUFFIXES})


# Set main include directory
FIND_PATH(OPENCV_INCLUDE_DIR opencv2/opencv.hpp PATHS ${OpenCV_CORE_INCLUDE_DIR}/..)

# Set main link libraries containing all required components
FOREACH(NAME ${OpenCV_FIND_REQUIRED_COMPONENTS})
    LIST(APPEND OPENCV_LIBRARIES "${OpenCV_${NAME}_LIBRARY}\;")
ENDFOREACH(NAME)
set(OPENCV_LIBRARIES ${OPENCV_LIBRARIES} CACHE PATH "")

MARK_AS_ADVANCED(
    OpenCV_ROOT_DIR
    OpenCV_CORE_INCLUDE_DIR
    OpenCV_HIGHGUI_INCLUDE_DIR
    OpenCV_IMGPROC_INCLUDE_DIR
    OpenCV_CORE_LIBRARY
    OpenCV_HIGHGUI_LIBRARY
    OpenCV_IMGPROC_LIBRARY
    OpenCV_LIBRARIES
)

find_package_handle_standard_args(
    opencv DEFAULT_MSG
    OPENCV_INCLUDE_DIR
    OpenCV_CORE_LIBRARY
    OpenCV_HIGHGUI_LIBRARY
    OpenCV_IMGPROC_LIBRARY
)
