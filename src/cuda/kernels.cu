#include "kernels.h"

#include "defines.h"


/******************************************************************************/
//	Datastructures
/******************************************************************************/
typedef float2 GridType[GRID_V][GRID_U][POLARIZATIONS];
typedef float3 UVWType[BASELINES][TIMESTEPS][CHANNELS];
typedef float2 VisibilitiesType[BASELINES][TIMESTEPS][CHANNELS][POLARIZATIONS];
typedef float2 SupportType[1][SUPPORT_V][OVERSAMPLE_V][SUPPORT_U][OVERSAMPLE_U];


/******************************************************************************/
//	Shared memory
/******************************************************************************/
__shared__ int4   shared_info[TIMESTEPS][CHANNELS];
__shared__ float2 shared_vis[TIMESTEPS][CHANNELS][POLARIZATIONS];


/******************************************************************************/
//	Helper functions
/******************************************************************************/
__device__ void atomicAdd(float2 *ptr, float2 sumXX, float2 sumXY, float2 sumYX, float2 sumYY) {
	atomicAdd(&ptr[0].x, sumXX.x);
	atomicAdd(&ptr[0].y, sumXX.y);
	atomicAdd(&ptr[1].x, sumXY.x);
	atomicAdd(&ptr[1].y, sumXY.y);
	atomicAdd(&ptr[2].x, sumYX.x);
	atomicAdd(&ptr[2].y, sumYX.y);
	atomicAdd(&ptr[3].x, sumYY.x);
	atomicAdd(&ptr[3].y, sumYY.y);
}

__device__ void addSupportPixel(float2 &sum, float2 supportPixel, float2 vis) {
	sum.x += supportPixel.x * vis.x;
	sum.y += supportPixel.x * vis.y;
	sum.x -= supportPixel.y * vis.y;
	sum.y += supportPixel.y * vis.x;
}


/******************************************************************************/
//	loadIntoSharedMem
/******************************************************************************/
__device__ void loadIntoSharedMem(
    const VisibilitiesType visibilities,
    const UVWType uvw,
    const uint2 supportPixelsUsed[BASELINES])
{
	unsigned bl = blockIdx.x;
	uint2    supportSize = supportPixelsUsed[bl];

	for (unsigned ch = threadIdx.x; ch < CHANNELS * TIMESTEPS; ch += blockDim.x) {
        float3   coords = uvw[bl][0][ch];

		#if MODE == MODE_SIMPLE
			unsigned u_int  = __float2int_rn(coords.x);
			unsigned v_int  = __float2int_rn(coords.y);
		#elif MODE == MODE_OVERSAMPLE || MODE == MODE_ATERM
			unsigned u_int  = __float2int_rz(coords.x);
			unsigned v_int  = __float2int_rz(coords.y);
			float    u_frac = coords.x - u_int;
			float    v_frac = coords.y - v_int;
		#endif

		#if MODE == MODE_SIMPLE
			shared_info[0][ch] = make_int4(-u_int % supportSize.x, -v_int % supportSize.y, 0, u_int + GRID_U * v_int);
		#elif MODE == MODE_OVERSAMPLE || MODE == MODE_ATERM
		#if ORDER == ORDER_W_OV_OU_V_U
			unsigned uv_frac_w_offset = (unsigned) 1 * SUPPORT_V * SUPPORT_U * OVERSAMPLE_V * OVERSAMPLE_U +
															  SUPPORT_U * SUPPORT_V * (OVERSAMPLE_U *
										(unsigned) (OVERSAMPLE_V * v_frac) + (unsigned) (OVERSAMPLE_U * u_frac));
		#elif ORDER == ORDER_W_V_OV_U_OU || MODE == MODE_ATERM
			unsigned uv_frac_w_offset = (unsigned) 1 * SUPPORT_V * OVERSAMPLE_V * SUPPORT_U * OVERSAMPLE_U +
										(unsigned) (OVERSAMPLE_V * v_frac) * SUPPORT_U * OVERSAMPLE_U +
										(unsigned) (OVERSAMPLE_U * u_frac);
		#endif
			shared_info[0][ch] = make_int4(-u_int % supportSize.x,
										   -v_int % supportSize.y,
										   uv_frac_w_offset,
										   u_int + GRID_U * v_int);
		#endif
	}

	for (unsigned i = threadIdx.x; i < CHANNELS * TIMESTEPS * POLARIZATIONS / 2; i += blockDim.x) {
		((float4 *) shared_vis)[i] = ((float4 *) visibilities[bl])[i];
	}
}


/******************************************************************************/
//	convolve
/******************************************************************************/
__device__ void convolve(
    const unsigned bl,
    GridType grid,
    const SupportType support,
    const uint2 supportPixelsUsed[BASELINES])
{
    uint2  supportSize = supportPixelsUsed[bl];

    for (int i = supportSize.x * supportSize.y - threadIdx.x - 1; i >= 0; i -= blockDim.x) {
        int box_u = - (i % supportSize.x);
        int box_v = - (i / supportSize.x);

		float2 sumXX = make_float2(0, 0);
		float2 sumXY = make_float2(0, 0);
		float2 sumYX = make_float2(0, 0);
		float2 sumYY = make_float2(0, 0);

		unsigned grid_point = threadIdx.x;

		for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
			int4 info = shared_info[0][ch];

			int my_support_u = box_u + info.x;
			int my_support_v = box_v + info.y;

			if (my_support_u < 0) {
				my_support_u += supportSize.x;
			}

			if (my_support_v < 0) {
				my_support_v += supportSize.y;
			}

            unsigned index_u = my_support_u;
            unsigned index_v = my_support_v;

			#if MODE == MODE_SIMPLE || (MODE == MODE_OVERSAMPLE && ORDER == ORDER_W_OV_OU_V_U)
				unsigned supportIndex = index_u + SUPPORT_U * index_v + info.z;
			#elif (MODE == MODE_OVERSAMPLE && ORDER == ORDER_W_V_OV_U_OU) || MODE == MODE_ATERM
				unsigned supportIndex = OVERSAMPLE_U * index_u + OVERSAMPLE_V * SUPPORT_U * OVERSAMPLE_U * index_v + info.z;
			#endif

            float2 supportPixel = support[0][0][0][0][supportIndex];

			unsigned new_grid_point = my_support_u + GRID_U * my_support_v + info.w;

			if (new_grid_point != grid_point) {
				atomicAdd((float2 *) &grid[0][grid_point][0], sumXX, sumXY, sumYX, sumYY);

				sumXX = make_float2(0, 0);
				sumXY = make_float2(0, 0);
				sumYX = make_float2(0, 0);
				sumYY = make_float2(0, 0);

				grid_point = new_grid_point;
			}

			addSupportPixel(sumXX, supportPixel, shared_vis[0][ch][0]);
			addSupportPixel(sumXY, supportPixel, shared_vis[0][ch][1]);
			addSupportPixel(sumYX, supportPixel, shared_vis[0][ch][2]);
			addSupportPixel(sumYY, supportPixel, shared_vis[0][ch][3]);
		}

		atomicAdd(&grid[0][grid_point][0], sumXX, sumXY, sumYX, sumYY);
	}
}


/******************************************************************************/
//	wprojection
/******************************************************************************/
__global__ __launch_bounds__(NR_THREADS_PER_BLOCK, MIN_BLOCKS_PER_MULTIPROCESSOR)
void wprojection(
    void *grid,
    const void *support,
    const void *visibilities,
    const void *uvw,
    const void *supportPixelsUsed)
{

    GridType *grid_                       = (GridType *) grid;
    const SupportType *support_           = (const SupportType *) support;
    const VisibilitiesType *visibilities_ = (const VisibilitiesType *) visibilities;
    const UVWType *uvw_                   = (const UVWType *) uvw;
    const uint2 *supportPixelsUsed_       = (const uint2 *) supportPixelsUsed;

    loadIntoSharedMem(*visibilities_, *uvw_, supportPixelsUsed_);
    __syncthreads();

	unsigned bl = blockIdx.x;
    convolve(bl, *grid_, *support_, supportPixelsUsed_);
}
