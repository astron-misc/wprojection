#include "kernels.h"

/******************************************************************************/
//	addGrids
/******************************************************************************/
void addGrids(
    GridType a,
    const GridType b[],
    const unsigned nrThreads)
{
#if defined __AVX__
for (int i = 0; i < GRID_V * GRID_U * POLARIZATIONS / 4; i ++) {
    __m256 sum = ((__m256 *) b)[i];

	for (unsigned g = 1; g < nrThreads; g ++) {
		sum = _mm256_add_ps(sum, ((__m256 *) b[g])[i]);
	}

	((__m256 *) a)[i] = sum;
}

#elif defined __SSE2__
#pragma omp parallel for schedule(static, 65536)
for (int i = 0; i < GRID_V * GRID_U * POLARIZATIONS / 2; i ++) {
	__m128 sum = ((__m128 *) b)[i];

	for (unsigned g = 1; g < nrThreads; g ++) {
		sum = _mm_add_ps(sum, ((__m128 *) b[g])[i]);
	}

	((__m128 *) a)[i] = sum;
}
#else
#pragma omp parallel for
for (int v = 0; v < GRID_V; v ++) {
	for (unsigned u = 0; u < GRID_U; u ++) {
		for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
			std::complex<float> sum = b[0][v][u][pol];

			for (unsigned g = 1; g < nrThreads; g ++) {
				sum += b[g][v][u][pol];
			}

			a[v][u][pol] += sum;
		}
	}
}
#endif
}



/******************************************************************************/
//	gridVisibilities
/******************************************************************************/
void gridVisibilities(
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
	#pragma omp parallel
	{
		GridType *localGrid = &grid[omp_get_thread_num()];

		#if MODE == MODE_OVERSAMPLE
		#pragma omp for schedule(dynamic)
		for (int bl = 0; bl < BASELINES; bl ++) {
			unsigned v_end  = supportPixelsUsed[bl].y;

			for (unsigned v = 0; v < v_end; v ++) {
				for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
					float uc = uvw[bl][0][ch].x;
					float vc = uvw[bl][0][ch].y;
					unsigned wc = (unsigned) uvw[bl][0][ch].z;
					unsigned grid_u = (unsigned) uc;
					unsigned grid_v = (unsigned) vc;
					float	   u_frac = uc - grid_u;
					float	   v_frac = vc - grid_v;
					unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
					unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
					unsigned u_end  = supportPixelsUsed[bl].x;

					#if defined __AVX__
					__m256 vis = _mm256_load_ps((const float *) &visibilities[bl][0][ch][0]);
					__m256 *gridptr = (__m256 *) &(*localGrid)[grid_v + v][grid_u][0];

					#if ORDER == ORDER_W_OV_OU_V_U
                    float2 *support_ptr = &support[wc][ov][ou][v][0];
					#elif ORDER == ORDER_W_V_OV_U_OU
					float2 *support_ptr = &support[w][v][ov][0][ou];
					#endif
					for (unsigned u = 0; u < u_end; u ++) {
						#if ORDER == ORDER_W_OV_OU_V_U
						__m256 weight_r = _mm256_set1_ps(support_ptr[u].x);
						__m256 weight_i = _mm256_set1_ps(support_ptr[u].y);
						#elif ORDER == ORDER_W_V_OV_U_OU
						__m256 weight_r = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].x);
						__m256 weight_i = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].y);
						#endif
						__m256 t7 = _mm256_mul_ps(weight_r, vis);
						__m256 t6 = _mm256_mul_ps(weight_i, vis);
						__m256 t8 = _mm256_permute_ps(t6, 0xB1);
						__m256 t9 = _mm256_addsub_ps(t7, t8);
						gridptr[u] = _mm256_add_ps(gridptr[u], t9);
					}
					#elif defined __SSE3__
					__m128 vis0 = _mm_load_ps((const float *) &visibilities[bl][0][ch][0]);
					__m128 vis1 = _mm_load_ps((const float *) &visibilities[bl][0][ch][2]);
					__m128 *gridptr = (__m128 *) &(*localGrid)[grid_v + v][grid_u][0];

					#if ORDER == ORDER_W_OV_OU_V_U
					std::complex<float> *support_ptr = &support[wc][ov][ou][v][0];
					#elif ORDER == ORDER_W_V_OV_U_OU
					std::complex<float> *support_ptr = &support[w][v][ov][0][ou];
					#endif
					for (unsigned u = 0; u < u_end; u ++) {
						#if ORDER == ORDER_W_OV_OU_V_U
						__m128 weight_r = _mm_set1_ps(support_ptr[u].real());
						__m128 weight_i = _mm_set1_ps(support_ptr[u].imag());
						#elif ORDER == ORDER_W_V_OV_U_OU
						__m128 weight_r = _mm_set1_ps(support_ptr[OVERSAMPLE_U * u].x);
						__m128 weight_i = _mm_set1_ps(support_ptr[OVERSAMPLE_U * u].y);
						#endif
						__m128 t70 = _mm_mul_ps(weight_r, vis0);
						__m128 t60 = _mm_mul_ps(weight_i, vis0);
						__m128 t71 = _mm_mul_ps(weight_r, vis1);
						__m128 t61 = _mm_mul_ps(weight_i, vis1);
						__m128 t80 = _mm_shuffle_ps(t60, t60, 0xB1);
						__m128 t81 = _mm_shuffle_ps(t61, t61, 0xB1);
						__m128 t90 = _mm_addsub_ps(t70, t80);
						__m128 t91 = _mm_addsub_ps(t71, t81);
						gridptr[2 * u    ] = _mm_add_ps(gridptr[2 * u    ], t90);
						gridptr[2 * u + 1] = _mm_add_ps(gridptr[2 * u + 1], t91);
					}
					#else
					float2 visXX = visibilities[bl][0][ch][0];
					float2 visXY = visibilities[bl][0][ch][1];
					float2 visYX = visibilities[bl][0][ch][2];
					float2 visYY = visibilities[bl][0][ch][3];

					for (unsigned u = 0; u < u_end; u ++) {
						#if ORDER == ORDER_W_OV_OU_V_U
						float2 weight = support[wc][ov][ou][v][u];
						#elif ORDER == ORDER_W_V_OV_U_OU
						float2 weight = support[wc][v][ov][u][ou];
						#endif
                        printf("u=%d\n", u);
						(*localGrid)[grid_v + v][grid_u + u][0] += visXX * weight;
						(*localGrid)[grid_v + v][grid_u + u][1] += visXY * weight;
						(*localGrid)[grid_v + v][grid_u + u][2] += visYX * weight;
						(*localGrid)[grid_v + v][grid_u + u][3] += visYY * weight;
					}
					#endif
				}
			}
		}

		#elif MODE == MODE_SIMPLE
		#pragma omp for schedule(dynamic)
		for (int bl = 0; bl < BASELINES; bl ++) {
			unsigned u_end  = supportPixelsUsed[bl].x;
			unsigned v_end  = supportPixelsUsed[bl].y;

			for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
				unsigned grid_u = (unsigned) nearbyintf(uvw[bl][0][ch].x);
				unsigned grid_v = (unsigned) nearbyintf(uvw[bl][0][ch].y);

				for (unsigned v = 0; v < v_end; v ++) {
					for (unsigned u = 0; u < u_end; u ++) {
						float2 weight = support[v][u];

						for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
							float2 prod   = visibilities[bl][0][ch][pol] * weight;
							float2 &pixel = (*localGrid)[grid_v + v][grid_u + u][pol];
							pixel += prod;
						}
					}
				}
			}
		}
		#endif
	}
}


