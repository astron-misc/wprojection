#include "visualize.h"

#include <limits>
#include <cmath>

#include <opencv2/contrib/contrib.hpp>
#include <opencv2/core/core.hpp>           // cv::Mat
#include <opencv2/highgui/highgui.hpp>     // cv::imread()

NamedWindow::NamedWindow(std::string name)
    : m_name(name)
{
    cv::namedWindow(name, CV_WINDOW_AUTOSIZE);
}

NamedWindow::~NamedWindow() {
    cv::destroyWindow(m_name);
}

void NamedWindow::display_matrix(
    size_t height,
    size_t width,
    std::complex<float> *data,
    std::string form,
    std::string colormap,
    int waitKeyTime)
{
    // Create float image with values in range [0.0f,1.0f]
    cv::Mat img(height, width, CV_32F);

    if (form == "abs") {
        double max_abs = -std::numeric_limits<double>::infinity();
        for (auto y = 0; y < height; ++y) {
            for (auto x = 0; x < width; ++x) {
                double val = fabs(data[x + y*width]);
                if (val > max_abs) max_abs = val;
            }
        }

        for (auto y = 0; y < img.rows; ++y) {
            for (auto x = 0; x < img.cols; ++x) {
                img.at<float>(y, x) = float( fabs(data[x + y*width]) / max_abs );
            }
        }
    }

    if (form == "log") {
        double max_log = -std::numeric_limits<double>::infinity();
        for (auto y = 0; y < height; ++y) {
            for (auto x = 0; x < width; ++x) {
                double val = log(fabs(data[x + y*width]) + 1);
                if (val > max_log) max_log = val;
            }
        }

        for (auto y = 0; y < img.rows; ++y) {
            for (auto x = 0; x < img.cols; ++x) {
                img.at<float>(y, x) = float( log(fabs(data[x + y*width]) + 1) / max_log );
            }
        }
    }

    // Apply colormap
    auto cmap = cv::COLORMAP_HOT;
    if (colormap == "autumn") {
       cmap = cv::COLORMAP_AUTUMN;
    } else if (colormap == "bone") {
       cmap = cv::COLORMAP_BONE;
    } else if (colormap == "cool") {
       cmap = cv::COLORMAP_COOL;
    } else if (colormap == "hsv") {
       cmap = cv::COLORMAP_HSV;
    } else if (colormap == "jet") {
       cmap = cv::COLORMAP_JET;
    } else if (colormap == "ocean") {
       cmap = cv::COLORMAP_OCEAN;
    } else if (colormap == "pink") {
       cmap = cv::COLORMAP_PINK;
    } else if (colormap == "rainbow") {
       cmap = cv::COLORMAP_RAINBOW;
    } else if (colormap == "spring") {
       cmap = cv::COLORMAP_SPRING;
    } else if (colormap == "summer") {
       cmap = cv::COLORMAP_SUMMER;
    } else if (colormap == "winter") {
       cmap = cv::COLORMAP_WINTER;
    }

    cv::Mat tmp_img;
    img.convertTo(tmp_img, CV_8U, 255.0, 0.0);
    cv::Mat color_img;
    cv::applyColorMap(tmp_img, color_img, cmap);

    // Display
    cv::imshow(m_name, color_img);
    cv::waitKey(waitKeyTime);
}

void plotGrid(
    void *ptr,
    const int grid_v,
    const int grid_u,
    const int polarizations)
{
    typedef std::complex<float> GridType[grid_v][grid_u][polarizations];
    GridType *grid = (GridType *) ptr;

    std::complex<float> grid_copy[grid_v][grid_u];

    #pragma omp parallel for collapse(2)
	for (unsigned v = 0; v < grid_v; v ++) {
		for (unsigned u = 0; u < grid_u; u ++) {
            grid_copy[u][v] = (*grid)[v][u][0];
        }
    }
    NamedWindow fig("grid");
    fig.display_matrix(grid_u, grid_v, (std::complex<float> *) grid_copy, "log", "jet");
}
