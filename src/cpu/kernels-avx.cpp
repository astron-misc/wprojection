#include "kernels.h"

/******************************************************************************/
//  addGrids
/******************************************************************************/
void addGrids(
    GridType a,
    const GridType b[],
    const unsigned nrThreads)
{
    #pragma omp parallel for
    for (int v = 0; v < GRID_V; v ++) {
    	for (unsigned u = 0; u < GRID_U; u ++) {
    		for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
                wpg::float2 sum = b[0][v][u][pol];

    			for (unsigned g = 1; g < nrThreads; g ++) {
    				sum += b[0][v][u][pol];
    			}

    			a[v][u][pol] += sum;
    		}
    	}
    }
}



/******************************************************************************/
//  wprojection
/******************************************************************************/
void wprojection(
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
    GridType *localGrid = &grid[omp_get_thread_num()];

    #if MODE == MODE_OVERSAMPLE
    #pragma omp for schedule(dynamic)
    for (int bl = 0; bl < BASELINES; bl ++) {
        unsigned v_end  = supportPixelsUsed[bl].y;

        for (unsigned v = 0; v < v_end; v ++) {
            for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
                float uc = uvw[bl][0][ch].x;
                float vc = uvw[bl][0][ch].y;
                unsigned wc = (unsigned) uvw[bl][0][ch].z;
                unsigned grid_u = (unsigned) uc;
                unsigned grid_v = (unsigned) vc;
                float      u_frac = uc - grid_u;
                float      v_frac = vc - grid_v;
                unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
                unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
                unsigned u_end  = supportPixelsUsed[bl].x;

                __m256 vis = _mm256_load_ps((const float *) &visibilities[bl][0][ch][0]);
                __m256 *gridptr = (__m256 *) &(*localGrid)[grid_v + v][grid_u][0];

                #if ORDER == ORDER_W_OV_OU_V_U
                float2 *support_ptr = &support[wc][ov][ou][v][0];
                #elif ORDER == ORDER_W_V_OV_U_OU
                float2 *support_ptr = &support[w][v][ov][0][ou];
                #endif
                for (unsigned u = 0; u < u_end; u ++) {
                    #if ORDER == ORDER_W_OV_OU_V_U
                    __m256 weight_r = _mm256_set1_ps(support_ptr[u].x);
                    __m256 weight_i = _mm256_set1_ps(support_ptr[u].y);
                    #elif ORDER == ORDER_W_V_OV_U_OU
                    __m256 weight_r = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].x);
                    __m256 weight_i = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].y);
                    #endif
                    __m256 t7 = _mm256_mul_ps(weight_r, vis);
                    __m256 t6 = _mm256_mul_ps(weight_i, vis);
                    __m256 t8 = _mm256_permute_ps(t6, 0xB1);
                    __m256 t9 = _mm256_addsub_ps(t7, t8);
                    gridptr[u] = _mm256_add_ps(gridptr[u], t9);
                }
            }
        }
    }

    #elif MODE == MODE_SIMPLE
    #pragma omp for schedule(dynamic)
    for (int bl = 0; bl < BASELINES; bl ++) {
        unsigned u_end  = supportPixelsUsed[bl].x;
        unsigned v_end  = supportPixelsUsed[bl].y;

        for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
            unsigned grid_u = (unsigned) nearbyintf(uvw[bl][0][ch].x);
            unsigned grid_v = (unsigned) nearbyintf(uvw[bl][0][ch].y);

            for (unsigned v = 0; v < v_end; v ++) {
                for (unsigned u = 0; u < u_end; u ++) {
                    float2 weight = support[v][u];

                    for (unsigned pol = 0; pol < POLARIZATIONS; pol ++) {
                        float2 prod   = visibilities[bl][0][ch][pol] * weight;
                        float2 &pixel = (*localGrid)[grid_v + v][grid_u + u][pol];
                        pixel += prod;
                    }
                }
            }
        }
    }
    #endif
}


/******************************************************************************/
//  awprojection
/******************************************************************************/
#if MODE == MODE_ATERM
void awprojection(
    const unsigned bl,
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES])
{
    unsigned v_end  = supportPixelsUsed[bl].y;

    GridType *localGrid = &grid[omp_get_thread_num()];

    for (unsigned v = 0; v < v_end; v ++) {
        for (unsigned ch = 0; ch < CHANNELS * TIMESTEPS; ch ++) {
            float uc = uvw[bl][0][ch].x;
            float vc = uvw[bl][0][ch].y;
            unsigned wc = (unsigned) uvw[bl][0][ch].z;
            unsigned grid_u = (unsigned) uc;
            unsigned grid_v = (unsigned) vc;
            float      u_frac = uc - grid_u;
            float      v_frac = vc - grid_v;
            unsigned ou     = (unsigned) (OVERSAMPLE_U * u_frac);
            unsigned ov     = (unsigned) (OVERSAMPLE_V * v_frac);
            unsigned u_end  = supportPixelsUsed[bl].x;

            __m256 vis = _mm256_load_ps((const float *) &visibilities[bl][0][ch][0]);
            __m256 *gridptr = (__m256 *) &(*localGrid)[grid_v + v][grid_u][0];
            float2 *support_ptr = &support[wc][v][ov][0][ou];

            for (unsigned u = 0; u < u_end; u ++) {
                __m256 weight_r = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].x);
                __m256 weight_i = _mm256_set1_ps(support_ptr[OVERSAMPLE_U * u].y);
                __m256 t7 = _mm256_mul_ps(weight_r, vis);
                __m256 t6 = _mm256_mul_ps(weight_i, vis);
                __m256 t8 = _mm256_permute_ps(t6, 0xB1);
                __m256 t9 = _mm256_addsub_ps(t7, t8);
                gridptr[u] = _mm256_add_ps(gridptr[u], t9);
            }
        }
    }
}
#endif
