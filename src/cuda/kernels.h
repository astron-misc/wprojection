#include "cu.h"

/******************************************************************************/
//	Definitions
/******************************************************************************/
#define MIN(A,B)						((A) < (B) ? (A) : (B))
#define NR_THREADS_PER_BLOCK			MIN(SUPPORT_U * SUPPORT_V, 1024)
#define MIN_BLOCKS_PER_MULTIPROCESSOR	(2048 / NR_THREADS_PER_BLOCK)


/******************************************************************************/
//	Kernels
/******************************************************************************/
__global__
void wprojection(
    void *grid,
    const void *support,
    const void *visibilities,
    const void *uvw,
    const void *supportPixelsUsed);
