#include <cstdio>
#include <complex>
#include <iomanip>

void printGrid(
    void *ptr,
    const int grid_v,
    const int grid_u,
    const int polarizations = 4)
{
    typedef std::complex<float> GridType[grid_v][grid_u][polarizations];
    GridType *grid = (GridType *) ptr;

    printf("Grid:\n");
    for (int v = 0; v < 5; v++) {
        for (int u = 0; u < 5; u++) {
            printf("%f,%f\t",
                (*grid)[GRID_V/2 + v][GRID_U/2 + u][0].real(),
                (*grid)[GRID_V/2 + v][GRID_U/2 + u][0].imag());
         }
         printf("\n");
     }
     printf("\n");
};

void print_parameters() {
    std::cout << " nr_stations: " << NR_STATIONS << std::endl
              << "nr_timeslots: " << NR_TIMESLOTS << std::endl
              << "nr_timesteps: " << NR_TIMESTEPS << std::endl
              << " nr_channels: " << CHANNELS << std::endl
              << "   grid_size: " << GRID_SIZE << std::endl
              << "support_size: " << SUPPORT_SIZE << std::endl
              << "  image_size: " << IMAGE_SIZE << std::endl
#if defined(ENABLE_ATERM)
              << "        mode: " << "AW-projection"
#else
              << "        mode: " << "W-projection"
#endif
              << std::endl;
}

void print_runtime(
    const char *name,
    double runtime)
{
    std::cout << std::setw(12)
              << name << ": "
              << std::fixed << std::setprecision(3)
              << runtime << " s"
              << std::endl;
}

void print_throughput(
    double runtime,
    int nr_baselines = BASELINES,
    int nr_timesteps = NR_TIMESTEPS,
    int nr_channels  = CHANNELS)
{
    unsigned long long nr_visibilities = nr_baselines * nr_timesteps * nr_channels;
    double throughput = 1e-6 * nr_visibilities / runtime;
    print_runtime("total", runtime);
    std::cout << std::setw(14)
              << "throughput: " << throughput << " MVisibilities/s"
              << std::endl;
}

void print_power(
    const char *name,
    double joules)
{
    std::cout << std::setw(14)
              << name << joules << " Joules"
              << std::endl;
}
