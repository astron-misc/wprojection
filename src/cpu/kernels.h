#include <complex>

#include <immintrin.h>
#include <omp.h>

#include "defines.h"
#include "types.h"

#define ALIGNMENT 64

using namespace wpg;

void addGrids(
    GridType a,
    const GridType b[],
    const unsigned nrThreads = 1);

void wprojection(
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES]);

void awprojection(
    const unsigned bl,
    GridType grid[],
    SupportType support,
    VisibilitiesType visibilities,
    UVWType uvw,
    uint2 supportPixelsUsed[BASELINES]);
