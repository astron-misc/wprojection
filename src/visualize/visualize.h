#include <string>
#include <complex>

#include "config.h"

#if defined(HAVE_OPENCV)
class NamedWindow {
    public:
        NamedWindow(std::string name);
        ~NamedWindow();

        void display_matrix(
            size_t height,
            size_t width,
            std::complex<float> *data,
            std::string form = "abs",
            std::string colormap = "hot",
            int waitKeyTime = 0);

    private:
        std::string m_name;
};

void plotGrid(
    void *ptr,
    const int grid_v,
    const int grid_u,
    const int polarizations = 4);
#else
void plotGrid(
    void *ptr,
    const int grid_v,
    const int grid_u,
    const int polarizations = 4)
{};
#endif
