#include <complex>
#include <omp.h>
#if MODE == MODE_ATERM
#include <fftw3.h>
#endif
#include <unistd.h>


#include "init.h"
#include "kernels.h"
#include "visualize.h"
#include "common.h"

#if defined(HAVE_POWERSENSOR)
#include "powersensor.h"
#endif

/******************************************************************************/
//  AW-Projection parameters
/******************************************************************************/
#define AWPROJECTION_INIT_KERNEL 0
#define AWPROJECTION_FFT_KERNEL  1

uint64_t get_total_system_memory()
{
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    return pages * page_size;
}

/******************************************************************************/
//	doCPU
/******************************************************************************/
void doCPU() {
    unsigned nr_threads;
    #pragma omp parallel
    nr_threads = omp_get_num_threads();

    // Allocate data structures
    std::cout << ">> Allocate data structures" << std::endl;
    GridType *tmpGrids = (GridType *) aligned_alloc(ALIGNMENT, nr_threads * sizeof(GridType));
    GridType *grid = (GridType *) aligned_alloc(ALIGNMENT, sizeof(GridType));
	VisibilitiesType *visibilities = (VisibilitiesType *) aligned_alloc(ALIGNMENT, sizeof(VisibilitiesType));
	uint2 supportPixelsUsed[BASELINES];
	float frequencies[CHANNELS];
    WPlanesType *w_planes = (WPlanesType *) aligned_alloc(ALIGNMENT, sizeof(WPlanesType));

    // Initialize data structures
    std::cout << ">> Initialize data structures" << std::endl;
    initVisibilities(visibilities);
    initSupportPixels(supportPixelsUsed);
	initFrequencies(frequencies);

    // Initialize uvw
    std::cout << ">> Initialize uvw" << std::endl;
    void *uvw_full = aligned_alloc(ALIGNMENT, BLOCKS * sizeof(UVWType));
    int nr_w_planes_max;
    initUVW(uvw_full, (void *) w_planes, &nr_w_planes_max, NR_STATIONS, BASELINES, NR_TIMESTEPS, NR_TIMESLOTS, CHANNELS, INTEGRATION_TIME, GRID_U, IMAGE_SIZE, frequencies);
    std::cout << " nr_w_planes: " << nr_w_planes_max << std::endl << std::endl;

    #if defined(HAVE_POWERSENSOR)
    powersensor::PowerSensor *powersensor;
    powersensor = powersensor::likwid::LikwidPowerSensor::create();
    powersensor::State power_states[2];
    power_states[0] = powersensor->read();
    #endif

    double runtime_gridder;
    double runtime_adder;

#if MODE == MODE_ATERM
    /*
     * AW-Projection
     */
    double total_runtime_support_init = 0;
    double total_runtime_support_fft = 0;
    double total_runtime_awprojection = 0;
    runtime_gridder = -omp_get_wtime();

    #pragma omp parallel
    {
        double runtime_support_init = 0;
        double runtime_support_fft = 0;
        double runtime_awprojection = 0;

        // Allocate
	    SupportType *support = (SupportType *) aligned_alloc(ALIGNMENT, nr_w_planes_max * sizeof(SupportType));

        // Create FFT plan
        #if AWPROJECTION_FFT_KERNEL
        fftwf_complex *data = (fftwf_complex *) support;
        fftwf_plan plan = fftwf_plan_dft_2d(
            SUPPORT_V * OVERSAMPLE_V,
            SUPPORT_U * OVERSAMPLE_U,
            data, data,
            FFTW_FORWARD, FFTW_ESTIMATE);
        #endif

        #pragma omp for
        for (unsigned bl = 0; bl < BASELINES; bl++) {
            for (unsigned block = 0; block < BLOCKS; block++) {
                // Get number of w-planes for current block
                int nr_w_planes = (*w_planes)[bl][block];

                // Compute aw-kernel
                #if AWPROJECTION_INIT_KERNEL
                runtime_support_init -= omp_get_wtime();
	            initSupport(*support, nr_w_planes);
                runtime_support_init += omp_get_wtime();
                #endif

                #if AWPROJECTION_FFT_KERNEL
                runtime_support_fft -= omp_get_wtime();
                for (unsigned w = 0; w < nr_w_planes; w++) {
                    fftwf_complex *data_ptr = data + ( w * SUPPORT_V * SUPPORT_U * OVERSAMPLE_V * OVERSAMPLE_U);
                    fftwf_execute_dft(plan, data_ptr, data_ptr);
                }
                runtime_support_fft += omp_get_wtime();
                #endif

                // Perform awprojection
                runtime_awprojection -= omp_get_wtime();
                UVWType *uvw_ = &(((wpg::UVWType *) uvw_full)[block]);
                awprojection(bl, tmpGrids, *support, *visibilities, *uvw_, supportPixelsUsed);
                runtime_awprojection += omp_get_wtime();
            }
       }

        #pragma omp critical
        {
            total_runtime_support_init += runtime_support_init;
            total_runtime_support_fft  += runtime_support_fft;
            total_runtime_awprojection += runtime_awprojection;
        }

        free(support);
    } // end parallel

    runtime_gridder += omp_get_wtime();

    // Normalize runtime for individual kernels
    double total_runtime_gridder = total_runtime_support_init +
                                   total_runtime_support_fft +
                                   total_runtime_awprojection;
    total_runtime_support_init /= total_runtime_gridder / runtime_gridder;
    total_runtime_support_fft  /= total_runtime_gridder / runtime_gridder;
    total_runtime_awprojection /= total_runtime_gridder / runtime_gridder;

    std::cout << "Kernel runtime:" << std::endl;
    print_runtime("init_support", total_runtime_support_init);
    print_runtime("fft_support", total_runtime_support_fft);
    print_runtime("awprojection", total_runtime_awprojection);

#else // end if USE_ATERM

    /*
     * W-Projection
     */
	SupportType *support = (SupportType *) aligned_alloc(ALIGNMENT, nr_w_planes_max * sizeof(SupportType));
    initSupport(*support, nr_w_planes_max);

    double runtime_wprojection = -omp_get_wtime();
    #pragma omp parallel
    {
        for (unsigned block = 0; block < BLOCKS; block++) {
            UVWType *uvw_ = &(((wpg::UVWType *) uvw_full)[block]);
            wprojection(tmpGrids, *support, *visibilities, *uvw_, supportPixelsUsed);
        }
    }
    runtime_wprojection += omp_get_wtime();

    std::cout << "Kernel runtime:" << std::endl;
    print_runtime("wprojection", runtime_wprojection);
    runtime_gridder = runtime_wprojection;
#endif

    runtime_adder = -omp_get_wtime();
	addGrids(*grid, tmpGrids, nr_threads);
    runtime_adder += omp_get_wtime();
    print_runtime("adder", runtime_adder);
    std::cout << std::endl;

    #if defined(HAVE_POWERSENSOR)
    power_states[1] = powersensor->read();
    #endif

    std::cout << ">> Results" << std::endl;
    print_throughput(runtime_gridder + runtime_adder);

    #if defined(HAVE_POWERSENSOR)
    print_power(" host: ",  powersensor::PowerSensor::Joules(power_states[0], power_states[1]));
    #endif

    // Plot grid
    plotGrid(grid, GRID_U, GRID_V);

    // Print grid
    #if defined(DEBUG)
    printGrid(grid, GRID_U, GRID_V);
    #endif

    // Free data structures
    free(tmpGrids);
    free(grid);
    free(visibilities);
    #if MODE != MODE_ATERM
    free(support);
    #endif
    free(uvw_full);
}


int main() {
    std::cout << ">> Parameters" << std::endl;
    print_parameters();

    doCPU();

    return EXIT_SUCCESS;
}
